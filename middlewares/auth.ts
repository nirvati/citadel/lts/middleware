import type {Next, Context} from 'koa';
import {STATUS_CODES} from '@runcitadel/utils';
import {isValidJwt} from "../utils/jwt.ts";
import * as diskLogic from "../logic/disk.ts";

export async function jwt(ctx: Context, next: Next): Promise<void> {
  const reqJwt = ctx.request.headers.authorization!.split(" ")[1];
  if (!reqJwt || typeof reqJwt !== "string") {
    ctx.throw(STATUS_CODES.BAD_REQUEST, "Missing or invalid authorization header");
  }

  const isValid = await isValidJwt(
    reqJwt as string,
    await diskLogic.readJWTPublicKeyFile(),
  );
  if (!isValid) {
    ctx.throw(STATUS_CODES.UNAUTHORIZED, "Invalid JWT");
  }
  await next();

}
