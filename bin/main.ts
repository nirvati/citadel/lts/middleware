#!/usr/bin/env -S node

/**
 * Module dependencies.
 */

import * as http from 'node:http';
import * as process from 'node:process';
import app from '../app.ts';

// If process.env.MANAGER_HOST is set, wait until we can reach it by fetching /ping
if (process.env.MANAGER_HOST) {
  const managerHost = process.env.MANAGER_HOST;
  const managerPort = process.env.MANAGER_PORT || '3000';
  const managerUrl = `http://${managerHost}:${managerPort}/ping`;

  console.log(`Waiting for manager at ${managerUrl}...`);

  const maxRetries = 30;
  const retryInterval = 1000;

  for (let i = 0; i < maxRetries; i++) {
    try {
      await fetch(managerUrl);
      break;
    } catch (e) {
      console.log(`Failed to reach manager at ${managerUrl}, retrying...`);
      await new Promise((resolve) => setTimeout(resolve, retryInterval));
    }
  }
}

/**
 * Get port from environment and store in Express.
 */

const port = normalizePort(process.env.PORT || '3000');

/**
 * Create HTTP server.
 */

const server = http.createServer(app.callback());

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(value: string) {
  const port = Number.parseInt(value, 10);

  if (Number.isNaN(port)) {
    // Named pipe
    return value;
  }

  if (port >= 0) {
    // Port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error: any) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;

  // Handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  const addr = server.address();
  const bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr!.port;
  console.log('Listening on ' + bind);
}
