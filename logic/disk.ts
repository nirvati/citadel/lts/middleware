import {join} from 'node:path';
import * as fs from 'node:fs/promises';
import constants from '../utils/const.ts';

export function readJWTPublicKeyFile(): Promise<string> {
  return fs.readFile(constants.JWT_PUBLIC_KEY_FILE, "utf-8");
}

export async function readLndRestHiddenService(): Promise<string> {
  return readHiddenService('lnd-rest');
}

export async function readLndGrpcHiddenService(): Promise<string> {
  return readHiddenService('lnd-grpc');
}

export async function readLndCert(): Promise<string> {
  return fs.readFile(constants.LND_CERT_FILE, "utf-8");
}

export async function readHiddenService(id: string): Promise<string> {
  if (!/^[\w-]+$/.test(id)) {
    throw new Error('Invalid hidden service ID');
  }

  const hiddenServiceFile = join(
    constants.TOR_HIDDEN_SERVICE_DIR,
    id,
    'hostname',
  );
  return fs.readFile(hiddenServiceFile, "utf-8").then((contents) => contents.trim());
}

export async function readLndAdminMacaroon(): Promise<Buffer> {
  return fs.readFile(constants.LND_ADMIN_MACAROON_FILE);
}
