import Router from '@koa/router';

import {errorHandler} from '@runcitadel/utils';
import * as auth from '../../../middlewares/auth.ts';

import * as lightningLogic from '../../../logic/lightning.ts';

const router = new Router({
  prefix: '/v1/lnd/address',
});

router.use(errorHandler);

router.get('/', auth.jwt, async (ctx, next) => {
  ctx.body = await lightningLogic.generateAddress();
  await next();
});

export default router;
