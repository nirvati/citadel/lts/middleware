import * as process from 'node:process';
import jwt from 'jsonwebtoken';
import * as diskLogic from '../logic/disk.ts';

const {verify} = jwt;

export async function getIdFromJwt(payload: string): Promise<string> {
  const pubkey = await diskLogic.readJWTPublicKeyFile();
  return new Promise((resolve, reject) => {
    verify(payload, pubkey, (error, decoded) => {
      if (error) {
        reject(new Error(`Invalid JWT: ${JSON.stringify(error)}`));
        // Make sure decoded exists and is an object and id is defined and is a string
      } else if (
        typeof decoded === 'object' &&
        typeof decoded.id === 'string'
      ) {
        resolve(decoded.id);
      } else {
        reject(new Error('Invalid JWT'));
      }
    });
  });
}

export async function isValidJwt(payload: string, pubkey: string): Promise<boolean> {
  return new Promise((resolve) => {
    verify(payload, pubkey, (error) => {
      if (error) {
        console.error(error);
        resolve(false);
      } else {
        resolve(true);
      }
    });
  });
}
