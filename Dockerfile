FROM oven/bun:1

WORKDIR /app

COPY . .

RUN bun install --production

ENTRYPOINT [ "bun", "run", "bin/main.ts" ]
